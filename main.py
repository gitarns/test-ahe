from prefect import task, flow, Flow

import time

@task
def hi():
    print("Hello, world!")
    
@task
def bye():
    time.sleep(5)
    print("Goodbye, world!")
    
@flow(name="my_workflow")
def my_workflow():
    hi()
    bye()

